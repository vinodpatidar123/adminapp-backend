const mongoose = require('mongoose');

const AdminSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: true,
        unique: true
    },
    lastName: {
        type: String,
        required: true
    },
    email: { 
        type: String,
        required: true,
        unique: true
    },
    password:{
        type:String,
        required:true
    },
    role: {
        type: String,
        required: true
    },
    blocked:{
        type :Boolean,
        default:false
    }
});


module.exports = mongoose.model('Admin', AdminSchema);