const express = require("express");
const router = express.Router();
const Authentication = require("../middlewares/authentication");
const AdminController = require("../controllers/AdminController");

router.post("/registerAdmin",AdminController.CreateAdmin);
router.post("/loginAdmin",AdminController.AdminLogin);
router.get("/admin",Authentication,AdminController.GetAdmin);
router.delete("/admin/:id",Authentication,AdminController.DeleteAdmin);
router.get("/admins",Authentication,AdminController.GetAdmins);
router.post("/blockUnblockAdmin",Authentication,AdminController.BlockUnblockAdmin);

module.exports = router;