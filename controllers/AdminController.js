var jwt = require('jsonwebtoken');
var bcrypt = require("bcrypt");
const Admin = require("../models/adminModel");

const CreateAdmin = async (req, res) => {
    let data = req.body;
    console.log(data);
    if(!data.firstName || !data.lastName){
        return res.status(400).json({success : false,message:"Please enter first and last name"});
    }
    if(!data.email){
        return res.status(400).json({success : false,message:"Please enter valid email"});
    }
    if(!data.role){
        return res.status(400).json({success : false,message:"Please enter valid role"});
    }
    let admin = await Admin.findOne({
      email: req.body.email
    });
    if (admin) return res.status(400).json({success : false,message:"Admin already exists"});
    else{
      admin = new Admin({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        role: req.body.role
      });
    }
    const salt = await bcrypt.genSalt(10);
    // @ts-ignore
    admin.password = await bcrypt.hash("pass123", salt);

    await admin.save(   );
    res.status(200).json({
      // @ts-ignore
      success : true,
      code : 200,
      message : "Admin Registerd"
    });
}

const AdminLogin = async (req,res)=>{
    let data = req.body;
    if(!data.email){
        return res.status(400).json({success : false,message:"Please enter valid email"});
    }
    if(!data.password){
        return res.status(400).json({success : false,message:"Please enter valid password"});
    }
    let user = await Admin.findOne({
        email: req.body.email
      });
    if (!user) return res.status(400).json({success : false,message:"User not exists"});

    // @ts-ignore
    bcrypt.compare(req.body.password, user.password, function(err, results){
    if(err){
        throw new Error("Error in Login");
        }
        if (results) {
            let payload = {
                id : user.id
            };
            let token = jwt.sign(payload,process.env.JWT_SECRET);
            res.setHeader('Authorization', token);
            res.setHeader('access-control-expose-headers', 'authorization');

            // @ts-ignore
            delete user.password;
            return res.status(200).json({success : true, message: "Login success",token:token,user:user})
        } else {
            return res.status(401).json({success : false, message: "Invalid Login" })
        }
    });
}

const GetAdmin = async (req,res)=>{
    console.log(req.user);
    await Admin.find({_id : req.user.id},(err,admin)=>{
        if(err){
            res.status(404).json({
                code : 404,
                success: false,
                message:err
            });
        }
        res.status(200).json({
            code : 200,
            success: true,
            admin: admin
        });
    })
}

const GetAdmins = async (req,res)=>{
    await Admin.find({},(err,admins)=>{
        if(err){
            res.status(404).json({
                code : 404,
                success: false,
                message:err
            });
        }
        res.status(200).json({
            code : 200,
            success: true,
            admins : admins
        });
    })
}

const BlockUnblockAdmin = async (req,res)=>{
    const { id, isBlocked } = req.body;

    await Admin.findByIdAndUpdate({_id : id},{
        blocked : isBlocked
    },(err,admin)=>{
        if(err){
            res.status(404).json({
                code : 404,
                success: false,
                message:err
            });
        }
        res.status(200).json({
            code : 200,
            success: true,
            admin: admin,
            message : "Admin Blocked/Unblocked"
        });
    })
}

const DeleteAdmin = async (req,res)=>{
    const { id } = req.params;

    await Admin.deleteOne({_id :id},(err,admin)=>{
        if(err){
            res.status(404).json({
                code : 404,
                success: false,
                message:err
            });
        }
        res.status(200).json({
            code : 200,
            success: true,
            message : "Admin Deleted"
        });
    })
}

// const UpdateAdminProfile = async (req,res)=>{
//     let user = await Admin.findOne({
//     email: req.body.email
//     });
//     if (!user) return res.status(400).json({message:"User not exists"});
//     // console.log(user);
//     res.statusCode(200).json(user);
    
// }

// const forgotPassword = async (req,res)=>{
//     let data = req.body;
//     console.log(data);
//     if(!data.email) return res.status(400).json({message : "Enter email address"});
//     let user = await Admin.findOne({
//         email : req.body.email
//     });
//     if(!user) return res.status(400).json({message : "User not exists"});

//     let randomOTP = Math.floor(100000 + Math.random() * 900000);

//     console.log(user);
//     await user.updateOne({_id : user.id}, {
//         $set: {
//             otp : 123456
//     }
//     // @ts-ignore
//     // @ts-ignore
//     },function (err, update) {
//         if (err) {
//             res.json(err);
//         }
//         else{
//             let payload = {
//                 id : user.id
//             };
//             let token = jwt.sign(payload,process.env.JWT_SECRET);
//             res.setHeader('Authorization', token);
//             res.setHeader('access-control-expose-headers', 'authorization');
//             // var mailOptions = {
//             // from: 'beyopib865@mailboxt.com',
//             // // @ts-ignore
//             // to:  user.email,
//             // subject: 'Request for Password Reset',
//             // text: `
//             // <p>Hey ${user.
// // @ts-ignore
//         //     username || user.email},</p>
//         //     <p>We heard that you lost your Void hacks() password. Sorry about that!</p>
//         //     <p>But don’t worry! You can use the OTP to reset your password:</p>
//         //     <h2>${randomOTP}
//         //     <p>If you don’t use this link within 1 hour, it will expire.</p>
//         //     <p>Do something outside today! </p>
//         //     <p>–Your friends at Backwoods</p>
//         //     `
//         //   };
          
//         //   transporter.sendMail(mailOptions, function(error, info){
//         //     if (error) {
//         //       console.log(error);
//         //       return res.status(400).json({message : error.message});
//         //     } else {
//         //       return res.status(200).send("Email sent: " + info.response);
//         //     }
//         //   });
//             res.status(200).json({mag:"OTP Send Successfully"});
//         }
//     });
// }

// const resetPassword = async (req,res)=>{
//     let data = req.body;

//     if(!data.newPassword) return res.status(400).json({message:"Please enter new Password"});
//     let user = await Admin.findOne({
//         _id : req.user.id
//       });
//     if(!user) return res.status(400).json({message : "User not exist"});
//     const salt = await bcrypt.genSalt(10);
//     await user.updateOne({_id : user.id}, {
//         $set: {
//             password : await bcrypt.hash(data.newPassword, salt)
//         }},function (err, update) {
//             if (err) {
//                return res.status(400).json({message:err});
//             }
//             else{
//                return res.status(200).json({message:"Password Reset"});
//             }
//         });
// }


// const checkOldPassword = async(req,res)=>{
//     let data = req.body;
//     if(!data.oldPassword) return res.status(400).json({message : "Enter password"});
//     let user = await Admin.findOne({
//         _id : req.user.id
//     });
//     // @ts-ignore
//     bcrypt.compare(req.body.password, user.oldPassword, function(err, results){
//         if(!results){
//             return res.status(400).json({message:"Enter correct old Password"});
//         }
//     });
// }

// const changePassword = async (req,res)=>{
//     let data = req.body;

//     if(!data.oldPassword && !data.newPassword) return res.status(400).json({message : "Enter password"});
//     let user = await Admin.findOne({
//         _id : req.user.id
//       });
//       if(!user) return res.status(400).json({message : "User not exist"});

//       // @ts-ignore
//       bcrypt.compare(req.body.oldPassword, user.password, async function(err, results){
//             // console.log(results);
//             if (results) {
//                 const salt = await bcrypt.genSalt(10);
//                 await user.updateOne({_id : user.id}, {
//                     $set: {
//                         password : await bcrypt.hash(data.newPassword, salt)
//                 }
//                 },function (err, update) {
//                     if (err) {
//                        return res.status(400).json({message:err});
//                     }
//                 });
//                 // @ts-ignore
//                 return res.status(200).json({ message: "Password Changed Succesfully",user:user.username})
//             } else {
//                 return res.status(401).json({ message: "Enter correct Old Password" })
//             }
//         });
// }


module.exports = {
    CreateAdmin,
    GetAdmin,
    AdminLogin,
    GetAdmins,
    BlockUnblockAdmin,
    DeleteAdmin
}